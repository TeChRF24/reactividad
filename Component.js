const Component = (function(){
    //Constructor del  Componente
    const Constructor = function(options){
        //El elemento
        this.el = options.el;
        //El estado
        this.data = options.data;
        //Funcion que genera el  template
        this.template = options.template; 
    }
//Agregar los metodos al prototipo del constructor del componente
const d = document;
//Render UI
Constructor.prototype.render = function(){
    const $el = d.querySelector(this.el);
    //Si no existe el elemento corta el codigo dentro de la  funcion
    if(!$el) return;
    //Le pasamos el state para que genere el template
    $el.innerHTML = this.template(this.data);
    console.log(this.data);
}
//Actualizar el State de forma reactiva
Constructor.prototype.setState = function(obj){
    for(let key in obj){
        //Si el estado original tiene la propiedad que viene en el objeto que le pasamos entonces reemplaza el valor de esa propiedad del state con el valor que viene en la propiedad del objeto que le pasamos
            if(this.data.hasOwnProperty(key)){
                this.data[key] = obj[key];
            }
        }
        this.render();
}
Constructor.prototype.getState = function(){
    return JSON.parse(JSON.stringify(this.data));
}
    return Constructor;
})();